
## Functions

This is a basic calculator, user can:

1. enter digits 0-9 by clicking the number buttons (keyboard input is forbidden),
2. see the number input and result displayed,
3. use decimal point,
4. compute with basic arithmetic operations (multiplication, division, addition, subtraction) with the regular operator precedence and associativity rules
 - eg. 1 + 2 * 3 - 5 / 2 = 1 + 6 - 2.5 = 4.5
5. use equal sign to compute the required expression,
6. use AC button to clear all computations.


---

## GUI

GUI code is based on code by Derek Banas http://www.newthinktank.com/2016/09/learn-program-22/


---

## PyTest unit tests

There are several PyTest unit test for two functions.