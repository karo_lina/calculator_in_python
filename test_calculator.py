import pytest

import calculator


# Tests for function is_operator_last()
class TestOperatorLast:

    def test_is_operator_last_01(self):
        assert calculator.Calculator.is_operator_last("AB/C") is False

    def test_is_operator_last_02(self):
        assert calculator.Calculator.is_operator_last("ABC/") is True

    def test_is_operator_last_03(self):
        assert calculator.Calculator.is_operator_last("ABC*") is True

    def test_is_operator_last_04(self):
        assert calculator.Calculator.is_operator_last("ABC-") is True

    def test_is_operator_last_05(self):
        assert calculator.Calculator.is_operator_last("ABC+") is True

    def test_is_operator_last_06(self):
        assert calculator.Calculator.is_operator_last("") is None


# Tests for function is_zero_last()
class TestZeroLast:
    def test_is_zero_last_01(self):
        assert calculator.Calculator.is_zero_last("123405") is False

    def test_is_zero_last_02(self):
        assert calculator.Calculator.is_zero_last("12340") is True

    def test_is_zero_last_03(self):
        assert calculator.Calculator.is_zero_last("0") is True

    def test_is_zero_last_04(self):
        assert calculator.Calculator.is_zero_last("") is None