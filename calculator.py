from tkinter import *
from tkinter import ttk


class Calculator:
    # Expression to compute, zero by default
    expression = "0"

    # Keeps track if the decimal point has been used already
    is_decimal = False

    # Keeps track if the last displayed value is the result of calculation
    is_result = False

    # True if the passed string ends with an operator
    @staticmethod
    def is_operator_last(string):
        if string:
            return string[-1] in ("/", "*", "-", "+")

    # True if the passed string ends with zero
    @staticmethod
    def is_zero_last(string):
        if string:
            return string[-1] == "0"

    # Updates the number display
    def update_display(self, string_to_display):
        self.number_entry.delete(0, "end")
        self.number_entry.insert(0, string_to_display)

    # Update expression
    def update_expression(self, remove_last_character, reassign, string_to_update):
        # remove_last_character = True -> remove it from expression
        if remove_last_character:
            self.expression = self.expression[:-1]

        # reassign True -> expression is set to a completely new value
        if reassign:
            self.expression = string_to_update

        # reassign False -> expression concatenated with the passed string
        else:
            self.expression += string_to_update

    # Called anytime a number button is pressed
    def number_pressed(self, number):
        # first input after launching the app/clearing all data/
        if self.expression == "0":

            # clear the entry field and display the number
            self.update_display(number)

            # remove the zero from expression and attach the entered number
            self.update_expression(True, False, number)

        # first number input after zero without decimal point following
        elif self.is_zero_last(self.expression) and self.is_operator_last(self.expression[:-1]):

            # clear the entry field and display the number
            self.update_display(number)

            # remove the zero from expression and attach the entered number
            self.update_expression(True, False, number)

        # first input after displaying result
        elif self.is_result:

            # clear the entry field and display the number
            self.update_display(number)

            # expression is refreshed - now it starts with the entered number
            self.update_expression(False, True, number)

            # reset the is_result flag to default
            self.is_result = False

        # first number after an operator
        elif self.is_operator_last(self.expression):

            # clear the entry field and display the number
            self.update_display(number)

            # attach the entered number at the end of the expression
            self.update_expression(False, False, number)

        # number inputted after decimal point or any digit 1-9
        else:
            # get currently displayed value
            current_value = self.entry_value.get()
            # attach the new number to it
            new_value = current_value + number

            # clear the entry field and display the new value
            self.update_display(new_value)

            # attach the entered number at the end of the expression
            self.update_expression(False, False, number)

    # Called anytime an operator button is pressed
    def operator_pressed(self, operator):
        # If the expression ends with an operator already, remove the old one
        if self.is_operator_last(self.expression):
            self.expression = self.expression[:-1]
            self.update_expression(True, False, "")

        # Attach the current operator to the expression
        self.update_expression(False, False, operator)

        # reset is_decimal flag to default
        self.is_decimal = False

        # if the operator is added after result display, set is_result to default
        if self.is_result:
            self.is_result = False

    # Called anytime an AC button is pressed
    def all_clear_pressed(self):

        # Set everything to default state
        self.is_decimal = False
        self.is_result = False
        self.update_display("0")
        self.update_expression(False, True, "0")

    # Called anytime a decimal point button is pressed
    def decimal_point_pressed(self):

        # if user clicks decimal point after the result display
        if self.is_result:

            # "0." is displayed and assigned to the expression
            self.update_display("0.")
            self.update_expression(False, True, "0")

            # set is_decimal flag to true
            self.is_decimal = True

            # reset is_result flag to default
            self.is_result = False

        # if user clicks decimal point after an operator
        elif self.is_operator_last(self.expression):
            # "0." is displayed and added to the expression
            self.update_display("0.")
            self.update_expression(False, False, "0.")

            #  set is_decimal flag to true
            self.is_decimal = True

        # if user clicks decimal point after number input
        # and the decimal point hasn't been used in the number already
        elif not self.is_operator_last(self.expression) and not self.is_decimal:

            # get current entry value, attach the decimal point to it, display it and add to the expression
            current_value = self.entry_value.get()
            new_value = current_value + "."
            self.update_display(new_value)
            self.update_expression(False, False, ".")

            #  set is_decimal flag to true
            self.is_decimal = True

    # Called anytime the equal sign button is pressed
    def equal_pressed(self):

        # if expression ends with an operator, remove the operator from the end of the expression
        if self.is_operator_last(self.expression):
            self.update_expression(True, False, "")

        # compute the result
        try:
            result = str(eval(self.expression))
            # display the result and set expression to result value

            # if the result is of type float, but in fact it is an integer, remove the decimal point and zero
            if result[-2:] == ".0":
                result = result[:-2]

            self.update_display(result)
            self.update_expression(False, True, result)

        # handle division by zero
        except ZeroDivisionError:
            # show error message
            self.update_display("Cannot divide by zero")
            # reset expression to default
            self.update_expression(False, True, "0")

        # handle floating point numbers overflow
        except OverflowError:
            # show error message
            self.update_display("Overflowed")
            # reset expression to default
            self.update_expression(False, True, "0")

        # set is_result flag to True
        self.is_result = True

        # reset is_decimal flag to default
        self.is_decimal = False

    def __init__(self, root):
        # Will hold the changing value stored in the entry
        self.entry_value = StringVar(root, value="0")

        # Define title for the app
        root.title("Calculator")

        # Defines the width and height of the window
        root.geometry("480x260")

        # Block resizing of Window
        root.resizable(width=True, height=True)

        # Customize the styling for the buttons and entry
        style = ttk.Style()
        style.configure("TButton",
                        font="Serif 15",
                        padding=10)

        style.configure("TEntry",
                        font="Serif 18",
                        padding=10)

        # Create the text entry box (with input from keyboard disabled)
        self.number_entry = ttk.Entry(root, textvariable=self.entry_value, width=50)

        self.number_entry.grid(row=0, columnspan=4, sticky = EW)

        self.number_entry.bind("<Key>", lambda e: "break")

        # ----- 1st Row -----

        self.button7 = ttk.Button(root, text="7", command=lambda: self.number_pressed('7')).grid(row=1, column=0)

        self.button8 = ttk.Button(root, text="8", command=lambda: self.number_pressed('8')).grid(row=1, column=1)

        self.button9 = ttk.Button(root, text="9", command=lambda: self.number_pressed('9')).grid(row=1, column=2)

        self.button_div = ttk.Button(root, text="/", command=lambda: self.operator_pressed('/')).grid(row=1, column=3)

        # ----- 2nd Row -----

        self.button4 = ttk.Button(root, text="4", command=lambda: self.number_pressed('4')).grid(row=2, column=0)

        self.button5 = ttk.Button(root, text="5", command=lambda: self.number_pressed('5')).grid(row=2, column=1)

        self.button6 = ttk.Button(root, text="6", command=lambda: self.number_pressed('6')).grid(row=2, column=2)

        self.button_mult = ttk.Button(root, text="*", command=lambda: self.operator_pressed('*')).grid(row=2, column=3)

        # ----- 3rd Row -----

        self.button1 = ttk.Button(root, text="1", command=lambda: self.number_pressed('1')).grid(row=3, column=0)

        self.button2 = ttk.Button(root, text="2", command=lambda: self.number_pressed('2')).grid(row=3, column=1)

        self.button3 = ttk.Button(root, text="3", command=lambda: self.number_pressed('3')).grid(row=3, column=2)

        self.button_add = ttk.Button(root, text="+", command=lambda: self.operator_pressed('+')).grid(row=3, column=3)

        # ----- 4th Row -----

        self.button_clear = ttk.Button(root, text="AC", command=lambda: self.all_clear_pressed()).grid(row=4, column=0)

        self.button0 = ttk.Button(root, text="0", command=lambda: self.number_pressed('0')).grid(row=4, column=1)

        self.button_decimal = ttk.Button(root, text=".", command=lambda: self.decimal_point_pressed()).grid(row=4, column=2)

        self.button_sub = ttk.Button(root, text="-", command=lambda: self.operator_pressed('-')).grid(row=4, column=3)

        # ---- 5th Row -----

        self.button_equal2 = ttk.Button(root, text="=", command=lambda: self.equal_pressed()).grid(row=5, columnspan=4, sticky=EW)


# Get the root window object
root = Tk()

# Create the calculator
calc = Calculator(root)

# Run the app until exited
root.mainloop()